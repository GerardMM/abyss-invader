﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinIndicator : MonoBehaviour
{
    private Text myText;
 
    void Update()
    {
        myText = this.gameObject.GetComponent<Text>();
       
        myText.text = GameManagerController._instance.collectiblesQuantity.ToString();   
    }
}
