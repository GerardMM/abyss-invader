﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //Control de colisiones
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "MainCharacter")
        {
            Destroy(GameObject.Find(gameObject.name));
        }
    }
}
