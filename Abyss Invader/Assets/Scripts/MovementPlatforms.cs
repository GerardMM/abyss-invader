﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlatforms : MonoBehaviour
{
    public float platformSpeed = 1f;
    public float move = 1f;
    float actualTime;

    void Start()
    {
        actualTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        var displacement = new Vector3(move, 0);

        transform.position += displacement * platformSpeed * Time.deltaTime;
        //Control de movimiento por tiempo
        if(Time.time > actualTime+1f)
        {
            move *= -1;
            actualTime = Time.time;
        }
    }
}
