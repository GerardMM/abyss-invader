﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanel : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private bool isPaused;

    

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            activateMenu();
        }
        else
        {
            deactivateMenu();
        }
        
    }
    //Control del canvas de pause, parada del juego y reactivacion
    void activateMenu()
    {
        this.pauseMenu.SetActive(true);
        Time.timeScale=0;
    }
    
    public void deactivateMenu()
    {
        this.pauseMenu.SetActive(false);
        Time.timeScale=1;
        isPaused = false;
    }
    
    }




