﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabEnemy : MonoBehaviour
{
    public float lifePoints = 5f;
    public float enemySpeed = 2f;
    public SpriteRenderer _mySpriteRenderer;
    public float move = 1f;
    Animator _myAnimator;
    Vector3 displacement;
   
    
    void Start()
    {
        _mySpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        _myAnimator = this.gameObject.GetComponent<Animator>();
    }

    
    void Update()
    {
        displacement = new Vector3(move, 0);
        transform.position += enemySpeed*displacement*Time.deltaTime;

    }
    
    //Control de collision
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14)
        {
            move *= -1;
            _myAnimator.SetFloat("Horizontal", displacement.x);
        }

        if (collision.gameObject.tag == "Bullet")
        {
            lifePoints -= 1;
        }

        if (lifePoints <= 0)
        {
            Destroy(gameObject);
        }
    }
}
