﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 2f;
    public Rigidbody2D _myrigidBody;
    public GameObject bulletHitEffect;
    // Start is called before the first frame update
    void Start()
    {
        _myrigidBody.velocity = transform.right * speed;
    }

    //Control de colisiones
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.layer == 14) {
            
        }
        else if (collision.gameObject.tag == "Missile")
        {
            
        }
        else
        {
            Instantiate(bulletHitEffect, transform.position, transform.rotation); 
            Destroy(gameObject);
        }
    }
}
