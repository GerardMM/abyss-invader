﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    public float lifePoints = 3;
    public float enemySpeed = 2f;
    private Transform target;
    public SpriteRenderer _mySpriteRenderer;
    float actualTime;
    float moverse = 1f;
    Animator _animator;
    
    void Start()
    {
        
        _mySpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        _animator = this.gameObject.GetComponent<Animator>();
        float actualTime = Time.time;
       
        
        //target = GameObject.FindGameObjectWithTag("Players");
    }

   
    void Update()
    {
        var displacement = new Vector3(moverse, 0);
        
        transform.position += displacement*enemySpeed*Time.deltaTime;
        //Movimiento por tiempo
        if(Time.time > actualTime+3f){
            moverse *= -1;
            actualTime = Time.time;
        }
        _animator.SetFloat("Horizontal", displacement.x);

    }
    
    //Control de collision
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            lifePoints -= 1;
        }

        if (lifePoints <= 0)
        {
            Destroy(gameObject);
        }
    }

}
