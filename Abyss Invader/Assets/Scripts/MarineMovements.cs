using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class MarineMovements : MonoBehaviour
{
    Vector3 position;
    public float marineSpeed = 2f;
    Animator animator;  
    private SpriteRenderer _mySpriteRenderer;
    Rigidbody2D _myRigidBody;
	Vector3 displacement;
    private bool fliped;

    public int collectibles = 0;
   
    Random rnd = new Random();
    private int ramdomPowerUp;
    public GameObject frontalSuperior;
    public GameObject frontalInferior;
    public GameObject superior;
    public GameObject inferior;
    public GameObject explosion;

    public AudioSource explosionEffect;
    public AudioSource coinEffect;
    public AudioSource powerUpEffect;



    public bool puZActived;
    public bool puOActived;
    public bool puTActived;


    private void Start()
    {
        _mySpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        animator = this.gameObject.GetComponent<Animator>();
        _myRigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        frontalSuperior.SetActive(false);
        frontalInferior.SetActive(false);
        superior.SetActive(false);
        inferior.SetActive(false);
        puZActived = true;
        puOActived = true;
        puTActived = true;
        fliped = false;
    }

    private void Update()
    {
        

        
    }

    void FixedUpdate(){
        
        displacement = new Vector3(Input.GetAxis("Horizontal"), 0);
        //Control del gamestate para moviemiento del personaje y acciones del mismo
        if (GameManagerController._instance.getGameStates() == GameStates.playing)
        {
            animator.SetFloat("Horizontal", displacement.x);
            transform.position += displacement * marineSpeed * Time.deltaTime;
        }
        
        if (displacement.x < 0 && transform.rotation.y <= 0f){
            FlipIzquierda();
        }

        if (displacement.x > 0 && transform.rotation.y > 0f)
        {
            FlipDerecha();
        }
        
        if (Input.GetKeyDown(KeyCode.Space)) ChangeGravity();
    }
    
    //Control de colisiones
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Colision contra Enemigos y pinchos/minas
        if (puZActived || puOActived || puTActived)
        {
            if (collision.gameObject.layer == 15)
            {
                if (collision.gameObject.tag == "mines")
                {
                    _myRigidBody.AddForce(new Vector2(200, 200));
                    Instantiate(explosion, collision.transform.position, collision.transform.rotation);
                    explosionEffect.Play();
                    Destroy(collision.gameObject);
                    
                }
                playerDeath();
            }
        }

        //Colision contra powerUps
        if (collision.gameObject.layer == 13)
        {
            if (puZActived || puOActived || puTActived)
            {
                powerUpEffect.Play();
                ramdomPowerUp = rnd.Next(0, 2);
                switch (ramdomPowerUp)
                {
                    case 0:
                        powerUpZero();
                        break;
                    case 1:
                        powerUpOne();
                        break;
                    default:
                        powerUpTwo();
                        break;

                }
            }
            else
            {
                Debug.Log("Todos los powerUps cogidos");
            }
        }

        //Colision contra monedas
        if (collision.gameObject.layer == 16)
        {
            coinEffect.Play();
            GameManagerController._instance.collectiblesQuantity += 1;
            if (GameManagerController._instance.collectiblesQuantity == 4)
            {
                 Debug.Log("4 MONEDAAAS");
                 GameManagerController._instance.collectiblesQuantity -= 4;
                 GameManagerController._instance.playersLife++;
            }
        }
        //Colision contra el casco de salida del primer nivel
        if (collision.gameObject.tag == "Helmet")
        {
            puZActived = true;
            puOActived = true;
            puTActived = true;
        }
        if (collision.gameObject.tag == "Ship")
        {
            Destroy(gameObject);
            GameManagerController._instance.setGameStates(GameStates.changeLevel);
        }
        //Colision cambio de level
        if (collision.gameObject.layer == 18)
        {
            GameManagerController._instance.setGameStates(GameStates.changeLevel);
        }
        
    }
    
    //cambio de gravedad
    private void ChangeGravity()
    {
        fliped = !fliped;

        _mySpriteRenderer.flipY = fliped;

        _myRigidBody.gravityScale *= -1;
    }
    
    //Cambio de estado del juego/Control de daños
    void playerDeath()
    {
        GameManagerController._instance.setGameStates(GameStates.gameOver);
    }

    
    //Eleccion de powerUps
    private void powerUpZero()
    {
        if (puZActived)
        {
            //Triple disparo
            frontalSuperior.SetActive(true);
            frontalInferior.SetActive(true);
            puZActived = !puZActived;
        }
        else
        {
            ramdomPowerUp = rnd.Next(0,1);
            switch (ramdomPowerUp)
            {
                case 0:
                    powerUpOne();
                    break;
                default:
                    powerUpTwo();
                    break;

            }
        }

    }

    //Activate triple shot power up
    private void powerUpOne()
    {
        if (puOActived)
        { 
            //Disparos diagonales
            superior.SetActive(true);
            inferior.SetActive(true);
            puOActived = !puOActived;
        }
        else
        {
            ramdomPowerUp = rnd.Next(0,1);
            switch (ramdomPowerUp)
            {
                case 1:
                    powerUpZero();
                    break;
                default:
                    powerUpTwo();
                    break;

            }
        }
    }

    //Activate improved gravity controll power up
    private void powerUpTwo()
    {
        if (puTActived)
        {
            _myRigidBody.gravityScale /= 5;
            puTActived = !puTActived;
        }
        else
        {
           ramdomPowerUp = rnd.Next(0,1);
           switch (ramdomPowerUp)
           {
               case 1:
                   powerUpZero();
                   break;
               default:
                   powerUpOne();
                   break;

           } 
        }
    }


    //Flip character right
    private void FlipDerecha()
        {
            transform.Rotate(0f, 180f, 0f);
           
        }

    //Flip character left
    private void FlipIzquierda()
        {
            transform.Rotate(0f, -180f, 0f);
        }

    //Delete bullet hit animated objects after animation is finished
    private void deleteBulletHits()
    {
        GameObject.FindGameObjectsWithTag("BulletHit");
        
    }
}
