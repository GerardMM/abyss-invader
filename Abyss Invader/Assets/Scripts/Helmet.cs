﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helmet : MonoBehaviour
{


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "MainCharacter")
        {
            Destroy(gameObject);
            GameManagerController._instance.setGameStates(GameStates.changeLevel);
        }
    }
}
