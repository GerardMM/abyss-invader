﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    playing,
    pause,
    gameOver,
    gameWin,
    gameStart, 
    changeLevel
}

public class GameManagerController : MonoBehaviour
{
    public static GameManagerController _instance;

    public GameStates gameState;

    public int playersLife = 10;

    public int collectiblesQuantity = 0;
    public String actualScene;
    
    public void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            Destroy(this);
        }
        gameState = GameStates.gameStart;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameStates getGameStates()
    {
        return this.gameState;
    }

    public void setGameStates(GameStates gameStates)
    {
        this.gameState = gameStates;
        gameStateUpdate();
    }

    // Update is called once per frame
    void Update()
    {
       gameStateUpdate();
    }

    public void gameStateUpdate()
    {
        switch (gameState)
        {
            case GameStates.pause:
                setInPause();
                break;
            case GameStates.gameOver:
                setInGameOver();
                break;
            case GameStates.gameWin:
                setInGameWin();
                break;
            case GameStates.gameStart:
                setInGameStart();
                break;
            case GameStates.changeLevel:
                setInChangeLevel();
                break;
            default:
                setInPlaying();
                break;

        }
    }

    private void setInGameStart()
    {
        GameManagerController._instance.setGameStates(GameStates.playing);
        playersLife = 5;
        collectiblesQuantity = 0;
        actualScene = SceneManager.GetActiveScene().name;
    }
    
    private void setInPlaying()
    {
        
    }

    private void setInGameWin()
    {
        SceneManager.LoadScene("WinScene");
        GameManagerController._instance.setGameStates(GameStates.playing);
    }

    private void setInGameOver()
    {
        if(this.playersLife >0)
        {
            this.playersLife--;

            GameManagerController._instance.setGameStates(GameStates.playing);
            
        }
        else
        {
            SceneManager.LoadScene("GameOverScene");
            GameManagerController._instance.setGameStates(GameStates.gameStart);
        }
    }

    private void setInChangeLevel(){
        if(SceneManager.GetActiveScene().name == "BossScene"){
            SceneManager.LoadScene("WinScene");
        }
        else if(SceneManager.GetActiveScene().name == "SecondLevelScene"){
            SceneManager.LoadScene("BossScene");
        }
        else if(SceneManager.GetActiveScene().name == "FirstLevelScene"){ 
            SceneManager.LoadScene("SecondLevelScene");
        }
        else if(SceneManager.GetActiveScene().name == "IntroScene"){
            SceneManager.LoadScene("FirstLevelScene");
        }

        GameManagerController._instance.setGameStates(GameStates.playing);
    }

    private void setInPause()
    {
       
    }
    
    
}
