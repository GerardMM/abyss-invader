﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainWeapon : MonoBehaviour
{
    public Transform aimingPosition;
    public GameObject bulletPrefab;
    public AudioSource shotEffect;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Shoot();
        }
    }

    //Disparos
    void Shoot()
    {
        shotEffect.Play();
        Instantiate(bulletPrefab, aimingPosition.position, aimingPosition.rotation);
    }
}
