﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform aimingPosition;
    public GameObject bulletPrefab;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Shoot();
        }
    }

    //Disparos
    void Shoot()
    {
        Instantiate(bulletPrefab, aimingPosition.position, aimingPosition.rotation);
    }
}
