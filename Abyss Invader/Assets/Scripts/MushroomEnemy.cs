﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomEnemy : MonoBehaviour
{
    public float lifePoints = 5f;
    public float damagePerHit = 1f;
    public float enemySpeed = 2f;
    public SpriteRenderer _mySpriteRenderer;
    public float move = 1f;
    public float rayLenght = 1f;
    Animator _myAnimator;
    Vector3 displacement;
    float actualTime;
    Rigidbody2D _myRigidBody;
    void Start()
    {
        _mySpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        _myAnimator = this.gameObject.GetComponent<Animator>();
        _myRigidBody = this.gameObject.GetComponent<Rigidbody2D>();

        actualTime = Time.time;
    }

    
    void Update()
    {
        //displacement = new Vector3(0, move);
        //transform.position += displacement*enemySpeed*Time.deltaTime;
        
        //Moviemiento por tiempo
        if(Time.time > actualTime+0.3f){
            _myRigidBody.gravityScale *= -1;
            actualTime = Time.time;
        }
    }

    //Control de colisiones
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            lifePoints -= 1;
        }

        if (lifePoints <= 0)
        {
            Destroy(gameObject);
        }
    }
}
