﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;

public class ButtonMenuController : MonoBehaviour
{
    private bool controlsOFF;
    //Cambio escena botones
    public void loadGameMode(string scene)
    {
        SceneManager.LoadScene(scene);
        GameManagerController._instance.setGameStates(GameStates.playing);
    }
    //Salir del juego
    public void quitGame()
    {
        Application.Quit();
    }
    
    public void loadTryAgain()
    {
        SceneManager.LoadScene(GameManagerController._instance.actualScene);
        GameManagerController._instance.setGameStates(GameStates.playing);
    }

    public void showControls(GameObject controls)
    {
        controlsOFF = !controlsOFF;
        if (!controlsOFF)
        {
            controls.SetActive(false);
        }
        else
        {
            controls.SetActive(true);
        }
    }
    
}
