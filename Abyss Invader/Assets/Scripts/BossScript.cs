﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    public float lifePoints = 20f;
    public Rigidbody2D _rb;
    public float actualTime;

    //Flag for attack patterns
    int attackToDo = 1;

    //Weapon objects position
    public Transform missileSpawnUp;
    public Transform missileSpawnCenter;
    public Transform missileSpawnDown;

    //Prefabs to instantiate
    public GameObject missileObject;

    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
        actualTime = Time.time;        
        
    }

    void Update()
    {
        if ((Time.time > actualTime + 2f) && (attackToDo == 1)){
            attackToDo = Random.Range(1, 6);
            actualTime = Time.time;
            FirstAttackPattern();
        }

        if ((Time.time > actualTime + 2f) && (attackToDo == 2))
        {
            attackToDo = Random.Range(1, 6);
            actualTime = Time.time;
            SecondAttackPattern();
        }

        if ((Time.time > actualTime + 2f) && (attackToDo == 3))
        {
            attackToDo = Random.Range(1, 6);
            actualTime = Time.time;
            ThirdAttackPattern();
        }

        if ((Time.time > actualTime + 2f) && (attackToDo == 4))
        {
            attackToDo = Random.Range(1, 6);
            actualTime = Time.time;
            FourthAttackPattern();
        }

        if ((Time.time > actualTime + 2f) && (attackToDo == 5))
        {
            attackToDo = Random.Range(1, 6);
            actualTime = Time.time;
            FifthAttackPattern();
        }
    }

    //Control de collision
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            lifePoints -= 1;
        }

        if (lifePoints <= 0)
        {
            Destroy(gameObject);
        }
    }

    void FirstAttackPattern()
    {
        Instantiate(missileObject, missileSpawnDown.position, missileSpawnDown.rotation);
    }

    void SecondAttackPattern()
    {
        Instantiate(missileObject, missileSpawnUp.position, missileSpawnUp.rotation);
        Instantiate(missileObject, missileSpawnDown.position, missileSpawnDown.rotation);
    }

    void ThirdAttackPattern()
    {
        Instantiate(missileObject, missileSpawnCenter.position, missileSpawnCenter.rotation); 
    }

    void FourthAttackPattern()
    {
        Instantiate(missileObject, missileSpawnCenter.position, missileSpawnCenter.rotation);
        Instantiate(missileObject, missileSpawnDown.position, missileSpawnDown.rotation);
    }

    void FifthAttackPattern()
    {
        Instantiate(missileObject, missileSpawnCenter.position, missileSpawnCenter.rotation);
        Instantiate(missileObject, missileSpawnUp.position, missileSpawnUp.rotation);
    }
}
